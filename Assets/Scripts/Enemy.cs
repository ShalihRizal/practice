﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

[SerializeField]
private int MaxHealth = 100, Damage, ExplosionDamage = 70;
public int CurrentHealth = 0, bulletSpeed = 500;

Player player;
Vector2 target;

public GameObject DeathEffect, bulletprefab;

[SerializeField]
private Transform FirePoint;


private void Awake() {
    CurrentHealth = MaxHealth;
    FirePoint = transform.GetChild(1);
}

    private void OnTriggerEnter2D(Collider2D other) {

        if (other.gameObject.tag == "Player")
        {
            player = other.GetComponent<Player>();
            player.TakeDamage(ExplosionDamage);

            Explode();

        }
    }

    private void Start() {
        InvokeRepeating("Shoot", 1, 1);
    }

    void Explode(){
        Destroy(gameObject);
        
        GameObject Effect = Instantiate (DeathEffect, transform.position, transform.rotation);
        Destroy(Effect, 3f);
    }

    private void Update() {

        Rotate();

        if (CurrentHealth <= 0)
        {
            Die();
        }

        target = new Vector2(GameObject.FindWithTag("Player").transform.position.x, GameObject.FindWithTag("Player").transform.position.y);

        transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), target, 1.5f *Time.deltaTime);
    }

    void Die(){

        Data.Star += Random.Range(10, 15);
        Data.Score += 100;
        Destroy(gameObject);

        GameObject Effect = Instantiate (DeathEffect, transform.position, transform.rotation);
        Destroy(Effect, 3f);

    }

    public void Shoot(){
        GameObject bullet = Instantiate (bulletprefab, FirePoint.position, FirePoint.rotation);
        Rigidbody2D bulletRb = bullet.GetComponent<Rigidbody2D>();
        bulletRb.AddForce(transform.right * bulletSpeed);

        Destroy(bullet, 7f);
    }
    

    void Rotate(){

        Quaternion rotation = Quaternion.LookRotation (GameObject.FindWithTag("Player").transform.position - transform.position, transform.TransformDirection(Vector3.up));
        transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);

    }

}
