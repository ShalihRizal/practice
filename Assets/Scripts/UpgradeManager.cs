﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpgradeManager : MonoBehaviour
{

    public TextMeshProUGUI scoreText, starsText, totalStarsText;

    private void Awake() {

        Data.TotalStars += Data.Star;

        scoreText.text = "Final Score : " + Data.Score.ToString();
        starsText.text = "Stars Collected : " + Data.Star.ToString();
        totalStarsText.text = "Total Stars : " + Data.TotalStars.ToString();


    }

    public void UpgradeDamage(){
        if (Data.TotalStars >= 10)
        {
            Data.TotalStars -= 10;
            Data.Damage +=10;
            totalStarsText.text = "Total Stars : " + Data.TotalStars.ToString();
        }else{
            Debug.Log("Need More Stars");
        }
    }

    public void UpgradeSpawnTime(){
        if (Data.TotalStars >= 30)
        {
            Data.TotalStars -= 30;

            if (Data.spawnTime <= 0.5f)
            {
                Debug.Log("Max Level");

            }else{

            Data.spawnTime -= 0.1f;
            totalStarsText.text = "Total Stars : " + Data.TotalStars.ToString();

            }

        }else{
            Debug.Log("Need More Stars");
        }
    }
}
