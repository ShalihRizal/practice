﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    int damage;

    private int HealthAmount, maxHealth = 100;

    private float moveSpeed = 5f, FireRate = 0.5f;

    public Transform FirePoint;

    private LineRenderer line;

    Camera cam;

    private Rigidbody2D rb;

    private Vector2 movement, mousePos;

    private void Awake() {

        Data.Score = 0;

        rb = gameObject.GetComponent<Rigidbody2D>();
        cam = Camera.main;

        HealthAmount = maxHealth;

        line = Camera.main.GetComponent<LineRenderer>();

        line.enabled = false;

        damage = Data.Damage;
    }

    private void Start() {
        StartCoroutine("Shot");
    }

    private void Update() {

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        if (HealthAmount <= 0)
        {
            Die(); 
        }
    }

    private void FixedUpdate() {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
        rb.rotation = angle;
    }

    public void Die(){
        Destroy(gameObject);
        SceneManager.LoadScene(2);
    }

    IEnumerator Shot()
    {
        while (true)
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(FirePoint.position, FirePoint.up);
		if(hitInfo){

			Enemy enemy = hitInfo.transform.GetComponent<Enemy>();
			
			if(enemy!= null){
				enemy.CurrentHealth -= damage;
			}
			
			// GameObject text = Instantiate (FloatingTextPrefab, hitInfo.point, Quaternion.identity);
			// Destroy(text, 2f);
			
			line.SetPosition(0, FirePoint.position);
			line.SetPosition(1, hitInfo.point);
			
		}else{
			line.SetPosition(0, FirePoint.position);
			line.SetPosition(1, FirePoint.position + FirePoint.up * 100);
		}
		
		line.enabled = true;
		
		yield return 0;
		
		line.enabled = false;

        yield return new WaitForSeconds(FireRate);

        }
		
    }

    public void TakeDamage(int amount){
        HealthAmount -= amount;
    }

}
