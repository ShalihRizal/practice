﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Player player;
    public int damage;

    private void Awake() {
        player = GameObject.FindWithTag("Player").GetComponent<Player>();
        damage = Data.Damage;
    }

    private void OnTriggerEnter2D(Collider2D other) {

        if (other.gameObject.tag == "Enemy")
        {
            
        }else if (other.gameObject.tag == "Player")
        {
            player.TakeDamage(damage);
            Destroy(gameObject);
        }
    }

    void OnBecameInvisible() {
         Destroy(gameObject);
     }

}
