﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CountdownTimer : MonoBehaviour
{
    [SerializeField] private float timeRemaining = 10;
    [SerializeField] private bool timerIsRunning = false;

    public TextMeshProUGUI TimerText;

    public TextMeshProUGUI scoreText;


    private void Start()
    {
        timerIsRunning = true;
    }

    void Update()
    {

        scoreText.text = "Score : " + Data.Score.ToString();

        if (timerIsRunning)
        {

            TimerText.text = "Time Remaining : " + timeRemaining.ToString("F0");

            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                timeRemaining = 0;
                timerIsRunning = false;
                SceneManager.LoadScene(2);
            }
        }
    }
}
